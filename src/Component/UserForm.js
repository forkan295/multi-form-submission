import React, { Component } from 'react'
import FormUserDetails from './FormUserDetails'
import FormPersonalDetails from './FormPersonalDetails';
import Confirm from './Confirm';
import Success from './Success';

 class UserForm extends Component {

    state = {
        step:1,
        firstName:'',
        lastName:'',
        email:'',
        occopation:'',
        city:'',
        bio:''
    }
    //proceed to next step
    nextStep = () =>{
        const {step} = this.state;
        this.setState({
            step:step+1
            
        })
    }
    FrontStep = () =>{
        const {step} = this.state;
        this.setState({
            step:1,
        firstName:'',
        lastName:'',
        email:'',
        occopation:'',
        city:'',
        bio:''
        })
    }
    //proceed to prev step
    PrevStep = () =>{
        const {step} = this.state;
        this.setState({
            step:step-1
        })
    }
    //handle field change
    handleChange = input => e =>
    {
          this.setState({[input]:e.target.value})
    }
    render() {
        const {step} = this.state;
        const {firstName,lastName,email,occopation,city,bio}= this.state;
        const values = {firstName,lastName,email,occopation,city,bio}
        switch(step){
            case 1:
                return(
                    <FormUserDetails 
                     nextStep = {this.nextStep}
                    handleChange = {this.handleChange}
                    values = {values}
                    />
                )
            case 2:
                return (
               
                    <FormPersonalDetails
                    nextStep = {this.nextStep}
                    PrevStep = {this.PrevStep}
                    handleChange = {this.handleChange}
                    values = {values}
                    />
                    
                    )    
            case 3:
                return (
                    <Confirm 
                    nextStep = {this.nextStep}
                    PrevStep = {this.PrevStep}
                    
                    values = {values}
                    />
                )    
            case 4:
                return <Success 
                FrontStep= {this.FrontStep}
                />    
                
        }
    }
}

export default UserForm
