import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

class FormUserDetails extends Component {
    continue = e =>{
        e.preventDefault()
        this.props.nextStep()
    }
    render() {
        const {values,handleChange} = this.props
        
        return (
           <MuiThemeProvider>
               <React.Fragment>
                   <AppBar title = "Enter user details" />
                   <TextField 
                   hintText='Hit your first name'
                   floatingLabelText = "first name"
                   onChange = {handleChange('firstName')}
                   defaultValue = {values.firstName}
                   />
                   <br/>
                   <TextField 
                   hintText='Hit your Last name'
                   floatingLabelText = "Last name"
                   onChange = {handleChange('lastName')}
                   defaultValue = {values.lastName}
                   />
                   <br/>
                   <TextField 
                   hintText='Hit your Email name'
                   floatingLabelText = "Email name"
                   onChange = {handleChange('email')}
                   defaultValue = {values.email}
                   />
                   <br/>
                   <RaisedButton 
                   label="continue"
                   secondary={true}
                //    style={styles.button}
                   onClick={this.continue}
                   style={styles.button}
                   />
               </React.Fragment>
           </MuiThemeProvider>
        )
    }
}
const styles = {
    button:{
        margin: 15
    }
}
export default FormUserDetails
