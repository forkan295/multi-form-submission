import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar'


class Success extends Component {
    Front = e =>{
        e.preventDefault()
        this.props.FrontStep()
    }
    render() {   
        return (
           <MuiThemeProvider>
               <React.Fragment>
                   <AppBar title = "Your submited successfuly" />
                 
                   <button
                   onClick = {this.Front}
                   >
                     Back to home
                   </button>
               </React.Fragment>
           </MuiThemeProvider>
        )
    }
}
const styles = {
    button:{
        margin: 15
    }
}
export default Success
