import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

class FormPersonalDetails extends Component {
    continue = e =>{
        e.preventDefault()
        this.props.nextStep()
    }
    back = e =>{
        e.preventDefault()
        this.props.PrevStep()
    }
    render() {
        const {values,handleChange} = this.props
        
        return (
           <MuiThemeProvider>
               <React.Fragment>
                   <AppBar title = "Enter Personal details" />
                   <TextField 
                   hintText='Hit your Occupation name'
                   floatingLabelText = "Occupation name"
                   onChange = {handleChange('occopation')}
                   defaultValue = {values.occopation}
                   />
                   <br/>
                   <TextField 
                   hintText='Hit your City name'
                   floatingLabelText = "City name"
                   onChange = {handleChange('city')}
                   defaultValue = {values.city}
                   />
                   <br/>
                   <TextField 
                   hintText='Hit your Bio name'
                   floatingLabelText = "Bio name"
                   onChange = {handleChange('bio')}
                   defaultValue = {values.bio}
                   />
                   <br/>
                   <RaisedButton 
                   label="continue"
                   secondary={true}
                //    style={styles.button}
                   onClick={this.continue}
                   style={styles.button}
                   />
                   <RaisedButton 
                   label="Back"
                   secondary={true}
                //    style={styles.button}
                   onClick={this.back}
                   style={styles.button}
                   />
               </React.Fragment>
           </MuiThemeProvider>
        )
    }
}
const styles = {
    button:{
        margin: 15
    }
}
export default FormPersonalDetails
