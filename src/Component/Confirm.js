import React, { Component } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar'
import {List,ListItem} from 'material-ui/List'
import RaisedButton from 'material-ui/RaisedButton'

class Confirm extends Component {
    continue = e =>{
        e.preventDefault()
        this.props.nextStep()
    }
    back = e =>{
        e.preventDefault()
        this.props.PrevStep()
    }
    render() {
        const {values:{firstName,lastName,email,occopation,city,bio}} = this.props
        
        return (
           <MuiThemeProvider>
               <React.Fragment>
                   <AppBar title = "Confirm User data" />
                  <List>
                      <ListItem 
                      primaryText = "First name"
                      secondaryText={firstName}
                      />
                      <ListItem 
                      primaryText = "Last name"
                      secondaryText={lastName}
                      />
                      <ListItem 
                      primaryText = "Email"
                      secondaryText={email}
                      />
                      <ListItem 
                      primaryText = "Occopation"
                      secondaryText={occopation}
                      />
                      <ListItem 
                      primaryText = "City"
                      secondaryText={city}
                      />
                      <ListItem 
                      primaryText = "Bio"
                      secondaryText={bio}
                      />
                  </List>
                   <RaisedButton 
                   label="continue & Confirm"
                   secondary={true}
                //    style={styles.button}
                   onClick={this.continue}
                   style={styles.button}
                   />
                   <RaisedButton 
                   label="Back"
                   secondary={true}
                //    style={styles.button}
                   onClick={this.back}
                   style={styles.button}
                   />
               </React.Fragment>
           </MuiThemeProvider>
        )
    }
}
const styles = {
    button:{
        margin: 15
    }
}
export default Confirm
